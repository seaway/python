import requests
from termcolor import *
us_list = ['gb_twtr', 'gb_jd', 'gb_qqq', 'gb_sohu', 'gb_yndx']
# title_text =('{0:10} {1:10} {2:10} {3:10} {4:10} {5:10} {6:10} {7:10}').format(
    # '股票名字', '当前价', '涨幅', '时间' , '涨跌', '开盘价', '最高价', '最低价'
title_text =('{0:15} {1:15} {2:15} {3:15} {4:15} {5:15} {6:15} {7:15}').format(
    'Stock Name', 'Current Price', 'Gain', 'Ups & Downs', 'Opening Price', 'Highest Price', 'Lowest Price', 'last Updated Date')
print(colored(title_text, "white"))
print(colored('- '*63, "white"))

for i in us_list:
    r = requests.get(url='http://hq.sinajs.cn/list='+i)
    res = r.text
    r1 = res.split('="')[1].split(',')
    price_text = '{0:<13} {1:<15} {2:<15} {4:<15} {5:<15} {6:<15} {7:<15} {3:<15}'.format(
        str(r1[0]), str(r1[1]), str(r1[2] + '%'), str(r1[3]),
        str(r1[4]), str(r1[5]), str(r1[6]), str(r1[7]))
    up_down_flag = float(r1[2])
    if(up_down_flag >= 0):
        print(colored(price_text, "green"))
    else:
        print(colored(price_text, "red"))